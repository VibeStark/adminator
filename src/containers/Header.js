import React, { Component } from 'react';

import Search from '../components/Search';
import SidebarToggle from '../components/SidebarToggle';

import User from '../components/User';

class Header extends Component{
    render(){
        return(
            <div className="header navbar">
                <div className="header-container">
                    <ul className="nav-left">
                        <li>
                            <SidebarToggle/>
                        </li>
                        <li>
                            <Search/>
                        </li>
                    </ul>
                    <ul className="nav-right">
                        <li>
                            <User/>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Header;
