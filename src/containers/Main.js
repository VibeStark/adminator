import React, { Component } from 'react';
import { Route } from 'react-router-dom';
const Dashboard = () => (
    <div>Dashboard</div>
);

const Email = () => (
    <div>Email</div>
);

const Compose = () => (
    <div>Compose</div>
);

const Calendar = () => (
    <div>Calendar</div>
);

class Main extends Component{
    render(){
        return(
            <main className="main-content bgc-grey-100">
                <Route exact path="/" component={Dashboard}/>
                <Route path="/email" component={Email}/>
                <Route path="/compose" component={Compose}/>
                <Route path="/calendar" component={Calendar}/>
            </main>
        )
    }
}

export default Main;
