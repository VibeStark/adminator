import React, { Component } from 'react';
import { connect } from 'react-redux';

import Header from './Header';
import Sidebar from './Sidebar';
import Main from './Main';
import Footer from './Footer'
import { BrowserRouter as Router } from 'react-router-dom';

class App extends Component {
    render(){
        let isCollapsed = this.props.state.isCollapsed ? 'is-collapsed' : '';
        return (
            <Router>
                <div className={`page ${isCollapsed}`}>
                    <Sidebar/>
                    <div className="page-container">
                        <Header/>
                        <Main/>
                        <Footer/>
                    </div>
                </div>
            </Router>
        )
    }
}

function mapStateToProps(state) {
    return {
        state: state.sidebarReducer
    }
}

export default connect(mapStateToProps)(App);
