import React, { Component } from 'react';

class Footer extends Component{
    render(){
        return(
            <footer className="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>Copyright © 2017 Designed by <a href="https://colorlib.com" title="Colorlib">Colorlib</a>. All rights reserved.</span>
            </footer>
        )
    }
}

export default Footer;
