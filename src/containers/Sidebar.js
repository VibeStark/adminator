import React, { Component } from 'react';
import SidebarMenu from '../components/SidebarMenu';
import SidebarLogo from "../components/SidebarLogo";

class Sidebar extends Component{
    render(){
        return (
            <div className="sidebar">
                <div className="sidebar-inner">
                    <SidebarLogo/>
                    <SidebarMenu/>
                </div>
            </div>
        )
    }
}

export default Sidebar;
