import { combineReducers } from 'redux';
import sidebarReducer from './sidebarReducer';

const rootReducers = combineReducers({
    sidebarReducer
});

export default rootReducers;
