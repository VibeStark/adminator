import * as types from '../constants/ActionTypes';

const initialState = {
    isCollapsed: true
};

const sidebarReducer = (state = initialState, action) => {
    if (action.type === types.TOGGLE_SIDEBAR){
        let old = state.isCollapsed;
        return {
            ...state,
            isCollapsed: !old
        }
    }
    return state;
};

export default sidebarReducer;
