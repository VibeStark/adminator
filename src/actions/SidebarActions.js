import * as types from '../constants/ActionTypes';

const toggleSidebar = () => {
    return {
        type: types.TOGGLE_SIDEBAR
    }
};

export default toggleSidebar;
