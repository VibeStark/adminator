import React, { Component } from 'react';

class Search extends Component{
    state = {
        active: false
    };

    toggleActive = () => {
        this.setState({
            active: !this.state.active
        })
    };

    searchInput = () => {
        return (
            <div className="search__input">
                <input className="form-control" type="text" placeholder="Search..."/>
            </div>
        )
    };
    render(){
        let active = this.state.active;
        let icon = active ? 'ti-close' : 'ti-search';
        return(
            <div className="search">
                <button onClick={this.toggleActive} className="search__toggle no-pdd-right">
                    <i className={`search__icon pdd-right-10 ${icon}`}></i>
                </button>
                {active && this.searchInput()}
            </div>
        )
    }
}

export default Search;
