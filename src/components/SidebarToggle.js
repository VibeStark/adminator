import React, { Component } from 'react';
import { connect } from 'react-redux';
import toggleSidebar from '../actions/SidebarActions';

class SidebarToggle extends Component{
    render(){
        return(
            <button className="sidebar-toggle" href="#" onClick={this.props.toggle}>
                <i className="ti-menu"></i>
            </button>
        )
    }
}

function mapStateToProps(state) {
    return {
        state: state
    }
}

function mapDispatchToProps(dispatch) {
    return {
        toggle: () => dispatch(toggleSidebar())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarToggle);
