import React, { Component } from 'react';
import { connect } from 'react-redux';
import db from '../db.json';
import toggleSidebar from '../actions/SidebarActions';

class SidebarLogo extends Component {
  render() {
    return (
      <div className="logo">
        <div className="logo__wrap">
          <div className="logo__inner">
            <div className="logo__pic">
              <img src={db.company.logo} alt="" />
            </div>
            <div className="logo__name">
              <h5 className="lh-1 mB-0">{db.company.name}</h5>
            </div>
          </div>
          <button className="logo__trigger" onClick={this.props.toggle}>
            <i className="ti-arrow-circle-left" />
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isCollapsed: state.sidebarReducer.isCollapsed,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggle: () => dispatch(toggleSidebar()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarLogo);
