import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import db from '../db.json';

const IconHolder = props => (
  <span className="icon-holder">
    <i className={`${props.color} ${props.icon}`} />
  </span>
);

class SidebarMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menu: db.navigation,
    };
  }


  render() {
    return (
      <ul className="sidebar-menu scrollable pos-r pT-30">
        {
          this.state.menu.map(item => (
            <li key={item.title}>
              <Route
                path={item.route}
                exact={item.active}
                children={({ match }) => (
                  <Link to={item.route}>
                    <span className={`sidebar-link ${match ? 'active' : ''}`}>
                      <IconHolder color={item.color} icon={item.icon} />
                      <span className="title">{item.title}</span>
                    </span>
                  </Link>
                )}
              />
            </li>
          ))
        }
      </ul>
    );
  }
}

export default SidebarMenu;
