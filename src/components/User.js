import React, { Component } from 'react';
import db from '../db.json';
import UserMenu from './UserMenu';

class User extends Component{
    state = {
        active: false,
        user: {
            name: db.user.name,
            avatar: db.user.avatar
        }
    };

    toggleActive = () => {
        this.setState({
            active: !this.state.active
        })
    };

    render(){
        let show = this.state.active ? 'show' : '';
        return(
            <div className="dropdown">
                <button className="dropdown-toggle no-after peers fxw-nw ai-c lh-1" onClick={this.toggleActive}>
                    <div className="peer mR-10">
                        <img className="w-2r bdrs-50p" src={this.state.user.avatar} alt=""/>
                    </div>
                    <div className="peer">
                        <span className="fsz-sm c-grey-900">{this.state.user.name}</span>
                    </div>
                </button>
                {show && <UserMenu/>}
            </div>
        )
    }
}

export default User;
