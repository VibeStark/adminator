import React, { Component } from 'react';

class UserMenu extends Component{
    render(){
        return(
            <ul className="dropdown-menu fsz-sm">
                <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                        <i className="ti-settings mR-10"></i>
                        <span>Setting</span>
                    </a>
                </li>
                <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                        <i className="ti-user mR-10"></i>
                        <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                        <i className="ti-email mR-10"></i>
                        <span>Messages</span>
                    </a>
                </li>
                <li role="separator" className="divider"></li>
                <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                        <i className="ti-power-off mR-10"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        )
    }
}

export default UserMenu;
